from api import app, db, jwt
from api.models import User, RevokedTokenModel
from api import views
from api.resources import auth, code_studio, subscriber, algorithm


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User}


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return RevokedTokenModel.is_jti_blacklisted(jti)


@jwt.user_loader_callback_loader
def user_loader_callback(identity):
    return User.query.get_or_404(identity)
