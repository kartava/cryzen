FROM python:3

ENV PYTHONUNBUFFERED 1

RUN mkdir /cryzen

WORKDIR /cryzen

ADD requirements.txt /cryzen/

RUN pip install -r requirements.txt

ADD . /cryzen/

ENV FLASK_ENV=development
