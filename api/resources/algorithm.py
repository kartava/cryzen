from flask import request
from flask_jwt_extended.utils import get_current_user
from flask_jwt_extended.view_decorators import jwt_required
from flask_restful import reqparse, Resource

from api import core
from api.common import status
from api.models import Algorithm


class AlgorithmAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', required=True)
        self.parser.add_argument('code', required=True)
        super(AlgorithmAPI, self).__init__()

    @staticmethod
    def _get_algorithm_by_pk(pk):
        request_user = get_current_user()
        params = {'user_id': request_user.id, 'id': pk}
        return Algorithm.query.filter_by(**params).first_or_404()

    @staticmethod
    def _get_algorithm_by_name(name):
        request_user = get_current_user()
        qs = Algorithm.query.filter_by(user_id=request_user.id)
        query = Algorithm.name.contains(name)
        return [algorithm.as_dict() for algorithm in qs.filter(query).all()]

    @staticmethod
    def _get_all_algorithms():
        request_user = get_current_user()
        return [algorithm.as_dict() for algorithm in request_user.algorithms]

    @jwt_required
    def get(self, pk=None):
        name = request.args.get('search')
        if pk:
            algorithm = self._get_algorithm_by_pk(pk)
            return {'results': algorithm.as_dict()}
        if name:
            return {'results': self._get_algorithm_by_name(name)}
        return {'results': self._get_all_algorithms()}

    @jwt_required
    def post(self):
        request_user = get_current_user()
        form_data = self.parser.parse_args()
        algorithm = Algorithm(user_id=request_user.id, **form_data)
        algorithm.save_to_db()
        return algorithm.detail, status.HTTP_201_CREATED

    @jwt_required
    def delete(self, pk):
        algorithm = self._get_algorithm_by_pk(pk)
        algorithm.delete()
        return {}, status.HTTP_204_NO_CONTENT


core.add_resource(AlgorithmAPI, '/algorithm', '/algorithm/<string:pk>')
