import requests
from flask import request
from flask_jwt_extended.view_decorators import jwt_required
from flask_restful import reqparse, Resource

from api import core
from api.common import status


class CodeStudioAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('code', required=True)
        self.handlers = {
            'save': self.save_handler,
            'build': self.build_handler
        }
        super(CodeStudioAPI, self).__init__()

    def get(self):
        return {}

    @staticmethod
    def save_handler(data):
        # implement interaction with container at `localhost:31415` here.
        return data

    @staticmethod
    def build_handler(data):
        url = 'http://52.207.232.103:31415/backtest'
        resp = requests.post(url, json=data)
        return resp.text

    def post(self):
        """
        Endpoint http://host:8000/api/code_studio?handler=save
        or http://host:8000/api/code_studio?handler=build
        :return:
        """
        handler = request.args.get('handler')
        form_data = self.parser.parse_args()
        if handler in self.handlers.keys():
            data = self.handlers.get(handler)(form_data)
            return {'data': data}, status.HTTP_201_CREATED
        error_msg = "Missing required query parameter `handler`."
        return {'detail': error_msg}, status.HTTP_400_BAD_REQUEST


core.add_resource(CodeStudioAPI, '/code_studio', methods=['GET', 'POST'])
