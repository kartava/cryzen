from flask import session
from flask_restful import reqparse, Resource

from flask_jwt_extended import (create_access_token, create_refresh_token,
                                jwt_refresh_token_required, get_jwt_identity,
                                jwt_required, get_raw_jwt,)

from api import core, facebook, google
from api.common import status
from api.common.utils import get_or_create
from api.models import User, RevokedTokenModel


class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity=current_user)
        return {'access_token': access_token}


class UserRegistration(Resource):
    required_params = {
        'help': 'This field cannot be blank',
        'required': True
    }

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('username', **self.required_params)
        self.parser.add_argument('email', **self.required_params)
        self.parser.add_argument('password', **self.required_params)
        super(UserRegistration, self).__init__()

    def post(self):
        form_data = self.parser.parse_args()
        username = form_data['username']
        email = form_data['email']
        password = form_data.pop('password')

        if User.find_by_username(username):
            return {'detail': 'User {} already exists'.format(username)}, \
                   status.HTTP_400_BAD_REQUEST
        if User.find_by_email(email):
            return {'detail': 'User {} already exists'.format(email)}, \
                   status.HTTP_400_BAD_REQUEST

        user = User(**form_data)
        user.set_password(password)

        try:
            user.save_to_db()
            access_token = create_access_token(identity=user.id)
            refresh_token = create_refresh_token(identity=user.id)
            return {
                'detail': 'User {} was created.'.format(username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }, status.HTTP_201_CREATED
        except:
            return {'detail': 'Something went wrong'}, \
                   status.HTTP_500_INTERNAL_SERVER_ERROR


class UserLogin(Resource):
    required_params = {
        'help': 'This field cannot be blank',
        'required': True
    }

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('username', **self.required_params)
        self.parser.add_argument('password', **self.required_params)
        super(UserLogin, self).__init__()

    def post(self):
        form_data = self.parser.parse_args()
        username = form_data['username']
        user = User.find_by_username(username)

        if not user:
            return {'detail': 'User {} doesn\'t exist'.format(user.id)}, \
                   status.HTTP_400_BAD_REQUEST

        if user.check_password(form_data['password']):
            access_token = create_access_token(identity=user.id)
            refresh_token = create_refresh_token(identity=user.id)
            return {
                'message': 'Logged in as {}'.format(user.username),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        else:
            return {'message': 'Wrong credentials'}, status.HTTP_400_BAD_REQUEST


class UserLogoutAccess(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Access token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, \
                   status.HTTP_500_INTERNAL_SERVER_ERROR


class UserLogoutRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti=jti)
            revoked_token.add()
            return {'message': 'Refresh token has been revoked'}
        except:
            return {'message': 'Something went wrong'}, \
                   status.HTTP_500_INTERNAL_SERVER_ERROR


class GoogleSocialAuth(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('token', required=True)
        super(GoogleSocialAuth, self).__init__()

    def post(self):
        form_data = self.parser.parse_args()
        session['google_token'] = (form_data['token'], '')
        me = google.get('userinfo')
        if me.status != status.HTTP_200_OK:
            return {'msg': 'Bad request.'}, status.HTTP_400_BAD_REQUEST
        data = {
            'google_id': me.data.get('id'),
            'username': me.data.get('name'),
            'email': me.data.get('email')
        }
        user = get_or_create(User, **data)
        return {
            'access_token': create_access_token(identity=user.google_id),
            'refresh_token': create_refresh_token(identity=user.google_id)
        }, status.HTTP_200_OK


class FacebookSocialAuth(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('token', required=True)
        super(FacebookSocialAuth, self).__init__()

    def post(self):
        form_data = self.parser.parse_args()
        session['facebook_token'] = (form_data['token'], '')
        me = facebook.get('/me', {'fields': 'id,email,name'})
        if me.status != status.HTTP_200_OK:
            return {'msg': 'Bad request.'}, status.HTTP_400_BAD_REQUEST
        data = {
            'facebook_id': me.data.get('id'),
            'username': me.data.get('name'),
            'email': me.data('email'),
        }
        user = get_or_create(User, **data)
        return {
            'access_token': create_access_token(identity=user.facebook_id),
            'refresh_token': create_refresh_token(identity=user.facebook_id)
        }, status.HTTP_200_OK


@google.tokengetter
def get_google_oauth_token():
    return session.get('google_token')


@facebook.tokengetter
def get_facebook_oauth_token():
    return session.get('facebook_token')


core.add_resource(TokenRefresh, '/auth/token/refresh', methods=['POST'])
core.add_resource(UserRegistration, '/auth/registration', methods=['POST'])
core.add_resource(UserLogin, '/auth/login', methods=['POST'])
core.add_resource(UserLogoutAccess, '/auth/logout/access', methods=['POST'])
core.add_resource(UserLogoutRefresh, '/auth/logout/refresh', methods=['POST'])
core.add_resource(GoogleSocialAuth, '/auth/google', methods=['POST'])
core.add_resource(FacebookSocialAuth, '/auth/facebook', methods=['POST'])
