from flask_restful import reqparse, Resource

from api import core
from api.common import status
from api.models import Subscriber


class SubscribeAPI(Resource):

    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('email', required=True)
        super(SubscribeAPI, self).__init__()

    def post(self):
        form_data = self.parser.parse_args()
        try:
            subscriber = Subscriber(**form_data)
        except ValueError as error:
            return {'msg': str(error)}, status.HTTP_400_BAD_REQUEST
        subscriber.save_to_db()
        return subscriber.detail, status.HTTP_201_CREATED


core.add_resource(SubscribeAPI, '/subscribe')
