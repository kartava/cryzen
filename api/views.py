from flask.templating import render_template

from api import app


@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def main_application(path):
    return render_template('main.html')
