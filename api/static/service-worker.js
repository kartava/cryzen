"use strict";
var precacheConfig = [["/index.html", "a768a6fa2d7f52bd7424f9166b44fb71"], ["/static/css/main.fe2be695.css", "ccbdb7c314435aa2331759cb73b94a76"], ["/static/media/background.9e226ed0.png", "9e226ed06f1adf86c9f1984e208c558d"], ["/static/media/close.40d683ed.svg", "40d683ed02baa57368022008e8f81109"], ["/static/media/container.aee792fb.png", "aee792fbf888c8b03a8c2d044263b970"], ["/static/media/content-polygon.ccba78cc.svg", "ccba78ccb81fdfb19b556a920e8c5120"], ["/static/media/credit-card.a0adb911.svg", "a0adb9119d94b96015b08b2f84449d67"], ["/static/media/dots-bg.65bb5aae.png", "65bb5aae0af506bb86decc40478b6f90"], ["/static/media/envelope.df73704a.svg", "df73704a17d2b5559f043e76a20d2b19"], ["/static/media/facebook.c0fd300d.svg", "c0fd300dac0729da15a474e3f48b1794"], ["/static/media/google.83a36a03.svg", "83a36a03009bce713e3b4634ec910fab"], ["/static/media/img_eight.78a172d5.png", "78a172d5a9513b67909e9a976b7257c2"], ["/static/media/img_five.d4329071.png", "d43290719e69a929e85d449e64d030e1"], ["/static/media/img_one.0a962089.png", "0a9620894b19f72a0c65aa8d26c80ea1"], ["/static/media/img_one.4c71af69.png", "4c71af692be2cc6ccf19df997b82d804"], ["/static/media/img_three.5d55f5e1.png", "5d55f5e1351095de00aa71e0d6bea8c7"], ["/static/media/img_two.38de0069.png", "38de0069306de3863bbbe8482dafd6fe"], ["/static/media/lock.bad13fac.svg", "bad13fac5a987ea9d196536a4a60f8dc"], ["/static/media/main-sphere.1188b0a9.png", "1188b0a94cec0a0d1132431217d380ff"], ["/static/media/map-location.a96ff480.svg", "a96ff4804ed36708ecc628ac828eb4fe"], ["/static/media/platform.f9323979.png", "f9323979c7d8178c8f7e21e4a87885eb"], ["/static/media/programmer.20a7f804.svg", "20a7f80415628ee7cbca15efc60068a1"], ["/static/media/timeline-polygon.ff4a65d9.svg", "ff4a65d9f90ec8746b18d80081438540"], ["/static/media/triangle.14083f19.svg", "14083f191ff3b87f75f1b8c0f302c39a"], ["/static/media/user.db307189.svg", "db3071896b7b15daa5d41d9e2c4e8969"]],
    cacheName = "sw-precache-v3-sw-precache-webpack-plugin-" + (self.registration ? self.registration.scope : ""),
    ignoreUrlParametersMatching = [/^utm_/],
    addDirectoryIndex = function (e, a) {
        var t = new URL(e);
        return "/" === t.pathname.slice(-1) && (t.pathname += a), t.toString()
    }, cleanResponse = function (a) {
        return a.redirected ? ("body" in a ? Promise.resolve(a.body) : a.blob()).then(function (e) {
            return new Response(e, {
                headers: a.headers,
                status: a.status,
                statusText: a.statusText
            })
        }) : Promise.resolve(a)
    }, createCacheKey = function (e, a, t, n) {
        var c = new URL(e);
        return n && c.pathname.match(n) || (c.search += (c.search ? "&" : "") + encodeURIComponent(a) + "=" + encodeURIComponent(t)), c.toString()
    }, isPathWhitelisted = function (e, a) {
        if (0 === e.length) return !0;
        var t = new URL(a).pathname;
        return e.some(function (e) {
            return t.match(e)
        })
    }, stripIgnoredUrlParameters = function (e, t) {
        var a = new URL(e);
        return a.hash = "", a.search = a.search.slice(1).split("&").map(function (e) {
            return e.split("=")
        }).filter(function (a) {
            return t.every(function (e) {
                return !e.test(a[0])
            })
        }).map(function (e) {
            return e.join("=")
        }).join("&"), a.toString()
    }, hashParamName = "_sw-precache",
    urlsToCacheKeys = new Map(precacheConfig.map(function (e) {
        var a = e[0], t = e[1], n = new URL(a, self.location),
            c = createCacheKey(n, hashParamName, t, /\.\w{8}\./);
        return [n.toString(), c]
    }));

function setOfCachedUrls(e) {
    return e.keys().then(function (e) {
        return e.map(function (e) {
            return e.url
        })
    }).then(function (e) {
        return new Set(e)
    })
}

self.addEventListener("install", function (e) {
    e.waitUntil(caches.open(cacheName).then(function (n) {
        return setOfCachedUrls(n).then(function (t) {
            return Promise.all(Array.from(urlsToCacheKeys.values()).map(function (a) {
                if (!t.has(a)) {
                    var e = new Request(a, {credentials: "same-origin"});
                    return fetch(e).then(function (e) {
                        if (!e.ok) throw new Error("Request for " + a + " returned a response with status " + e.status);
                        return cleanResponse(e).then(function (e) {
                            return n.put(a, e)
                        })
                    })
                }
            }))
        })
    }).then(function () {
        return self.skipWaiting()
    }))
}), self.addEventListener("activate", function (e) {
    var t = new Set(urlsToCacheKeys.values());
    e.waitUntil(caches.open(cacheName).then(function (a) {
        return a.keys().then(function (e) {
            return Promise.all(e.map(function (e) {
                if (!t.has(e.url)) return a.delete(e)
            }))
        })
    }).then(function () {
        return self.clients.claim()
    }))
}), self.addEventListener("fetch", function (a) {
    if ("GET" === a.request.method) {
        var e,
            t = stripIgnoredUrlParameters(a.request.url, ignoreUrlParametersMatching),
            n = "index.html";
        (e = urlsToCacheKeys.has(t)) || (t = addDirectoryIndex(t, n), e = urlsToCacheKeys.has(t));
        var c = "/index.html";
        !e && "navigate" === a.request.mode && isPathWhitelisted(["^(?!\\/__).*"], a.request.url) && (t = new URL(c, self.location).toString(), e = urlsToCacheKeys.has(t)), e && a.respondWith(caches.open(cacheName).then(function (e) {
            return e.match(urlsToCacheKeys.get(t)).then(function (e) {
                if (e) return e;
                throw Error("The cached response that was expected is missing.")
            })
        }).catch(function (e) {
            return console.warn('Couldn\'t serve response for "%s" from cache: %O', a.request.url, e), fetch(a.request)
        }))
    }
});