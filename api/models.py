import re
from datetime import datetime

from sqlalchemy.orm.mapper import validates
from werkzeug.security import generate_password_hash, check_password_hash

from api import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    google_id = db.Column(db.Integer, nullable=True)
    facebook_id = db.Column(db.Integer, nullable=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(255))
    algorithms = db.relationship("Algorithm", back_populates="user")

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    @classmethod
    def find_by_username(cls, username):
        return cls.query.filter_by(username=username).first()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email=email).first()

    def is_google_user(self):
        return bool(self.google_id)

    def is_facebook_user(self):
        return bool(self.facebook_id)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class RevokedTokenModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(120))

    def add(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def is_jti_blacklisted(cls, jti):
        query = cls.query.filter_by(jti=jti).first()
        return bool(query)


class Subscriber(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String)

    @validates('email')
    def validate_email(self, key, email):
        if not email:
            raise ValueError('No email provided')
        pattern = '(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)'
        if not re.match(pattern, email):
            raise ValueError('Provided email is not an email address')
        return email

    @property
    def detail(self):
        return {
            'id': self.id,
            'email': self.email
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class Algorithm(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    code = db.Column(db.Text)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    created_at = db.Column(db.DateTime, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, onupdate=datetime.utcnow)
    user = db.relationship(User, back_populates="algorithms")

    def __repr__(self):
        return '<Algorithm id:{}, name:{}>'.format(self.id, self.name)

    @classmethod
    def find_by_name(cls, name):
        query = cls.name.contains(name)
        return cls.query.filter(query).all()

    @property
    def detail(self):
        return {
            'id': self.id,
            'name': self.name,
            'code': self.code
        }

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def as_dict(self):
        data = {}
        for column in self.__table__.columns:
            if column.name in ('created_at', 'updated_at'):
                data[column.name] = str(getattr(self, column.name))
            else:
                data[column.name] = getattr(self, column.name)
        # return {c.name: getattr(self, c.name) for c in self.__table__.columns}
        return data

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
