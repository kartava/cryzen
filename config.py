import os

BASE_DIR = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SQLITE_DB_URL = 'sqlite:///' + os.path.join(BASE_DIR, 'app.db')
    SECRET_KEY = os.environ.get('SECRET_KEY', '1wef5f_df#dfsg4lDew8rh_xbmDEF')
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', 'remove_this_on_production')
    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', SQLITE_DB_URL)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    GOOGLE_ID = os.environ.get('GOOGLE_ID')
    GOOGLE_SECRET = os.environ.get('GOOGLE_SECRET')
    FACEBOOK_ID = os.environ.get('FACEBOOK_ID')
    FACEBOOK_SECRET = os.environ.get('FACEBOOK_SECRET')
